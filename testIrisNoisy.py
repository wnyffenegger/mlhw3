from learner import Learner
from learner import Classifier
from utils import Examples
from utils import IRIS_ATTR
from utils import IRIS_TRAINING
from utils import IRIS_TEST

import random

random.seed(42)

LEARNING_RATE = 0.1     # How much of error to adjust for
MOMENTUM = 0.02         # Momentum constant, how much of previous weight change to add to current weight change
TRIALS = 500            # Maximum number of iterations through all examples
OVERFIT = 5            # Percentage overfit to accept
HIDDEN = 3              # Number of hidden units in hidden layer
VALIDATE = True         # Should a validation set be used to test for overfitting


def accuracy(examples, classifier, train=True):
    """
    :param examples: set of example data
    :type examples: Examples
    :param classifier: initialized classifier to calculate accuracy of over training data
    :type classifier: Classifier
    :param train: if true find accuracy on training set
    :type train: bool
    :return: percent accuracy rounded to nearest percent
    :rtype: int
    """
    correct = 0.0
    data = examples.train
    if not train:
        data = examples.test

    for ex in data:
        _, _, outputs, _ = classifier.classify(ex)

        classification = None
        prediction = None

        for index, attr_name in enumerate(classifier.attr.outputs):
            if outputs[index] == 1:
                classification = attr_name.split('_')[1].center(15)
                break

        for attr_name in classifier.attr.outputs:
            if ex[attr_name] == 1:
                prediction = attr_name.split('_')[1].center(15)

        if classification == prediction:
            correct += 1.0
    return 100.0 * correct / len(data)


def corrupt(examples, percent):
    """
    Given a set of examples corrupt a defined percentage of them and return the number of corrupted examples
    :param examples: Examples
    :type examples: Examples
    :param percent: number of examples to corrupt as integer
    :type percent: int
    :return:
    """

    # build range to choose indices in training data to corrupt
    # without replacement
    indices = [k for k in range(0, len(examples.train))]

    # list possible values of target attribute to corrupt data by changing
    values = examples.attr.outputs

    to_corrupt = int(round(len(examples.train) * percent / 100))

    for j in range(0, to_corrupt):
        # Find index of example to corrupt
        choice = random.choice(indices)

        # Find current value of training example to change
        current_value = None
        for attr_name in values:
            if examples.train[choice][attr_name]:
                current_value = attr_name

        # filter out current value from list of choices and choose value to corrupt to
        corruptible = [target_value for target_value in values if current_value != target_value]
        corrupted_value = random.choice(corruptible)

        # corrupt same examples for both sets of data
        examples.train[choice][current_value] = 0
        examples.train[choice][corrupted_value] = 1

        # do not consider index again
        indices.remove(choice)

    return to_corrupt


if __name__ == "__main__":
    for i in range(1, 11):
        # load fresh data
        examplesNoValidation = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST, False)   # data for running with no validation
        examplesValidation = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST, True)      # data for runninw with validation

        noValidationCorrupted = corrupt(examplesNoValidation, 2 *i)
        validationCorrupted = corrupt(examplesValidation, 2 * i)

        print 'Training neural net with Iris examples, %d%% corrupted examples, %d hidden units,' \
              'a learning rate of %.3f, a momentum of %.3f, and %d trials' % (i * 2, HIDDEN, LEARNING_RATE, MOMENTUM, TRIALS)

        # separate learners with and without validation
        learnerNoValidation = Learner(examplesNoValidation, HIDDEN, LEARNING_RATE, MOMENTUM, TRIALS)
        learnerValidation = Learner(examplesValidation, HIDDEN, LEARNING_RATE, MOMENTUM, TRIALS)

        # Train nets with and without OVERFIT measure
        learnerNoValidation.run()
        learnerValidation.run(OVERFIT)

        # Create classifiers for resulting nets
        noValidation = Classifier(examplesNoValidation.attr, learnerNoValidation.hweights, learnerNoValidation.oweights)
        validation = Classifier(examplesValidation.attr, learnerValidation.hweights, learnerValidation.oweights)

        # Calculate accuracy on training data of classifier with no validation set and with validation set
        noValidationAccuracy = accuracy(examplesNoValidation, noValidation)
        noValidationAccuracyTest = accuracy(examplesNoValidation, noValidation, False)
        validationAccuracy = accuracy(examplesValidation, validation)
        validationAccuracyTest = accuracy(examplesValidation, validation, False)

        # print accuracy
        print '%d corrupted examples: \n\t\tw/o validation: %d%% accurate on training %d%% accurate on test\n\t\twith' \
              ' validation: %d%% accurate on training %d%% on test'\
              % (i * 2, noValidationAccuracy, noValidationAccuracyTest, validationAccuracy, validationAccuracyTest)


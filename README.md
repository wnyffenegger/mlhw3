
# ML HW 3 Feed Forward Neural Network with Back Propagation

# Files

* utils.py: utility methods for loading data and formatting it for the neural net
* learner.py: neural net learner with entropy, feed forward, back propagation, and classifier using weights
* testIdentity.py: run against identity data set using three and four hidden units
* testTennis.py: run against tennis data set using three hidden units
* testIris.py: run against iris data set using three hidden units
* testIrisNoisy.py: run against iris data set using 2, 4 ... 20% corrupted examples

# Modules

* utils.py
    * Attribute: load attributes and convert necessary attributes to binary and one of n attributes. Record the input
    attributes, output attributes, and mappings from converted forms back to original input forms
    * Examples: load all examples in Attribute given form
* learner.py:
    * methods for entropy, finding error in output and hidden units, and performing feed forward
    * Learner: constructing layers, conducting back propagation, and updating weights
    * Classifier: takes hidden layer weights and output layer weights then uses feed forward to classify individual examples

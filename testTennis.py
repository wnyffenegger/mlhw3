from learner import Learner
from learner import Classifier
from utils import Examples
from utils import TENNIS_ATTR
from utils import TENNIS_TRAINING
from utils import TENNIS_TEST

LEARNING_RATE = 0.1
MOMENTUM = 0.02
TRIALS = 3000

print 'Training neural net with identity examples, a learning rate of %.3f, a momentum of %.3f, and %d trials' % (LEARNING_RATE, MOMENTUM, TRIALS)

examples = Examples(TENNIS_ATTR, TENNIS_TRAINING, TENNIS_TEST)
three_unit = Learner(examples, 3, LEARNING_RATE, MOMENTUM, TRIALS)

# Training nets
print 'Running SGD for three hidden unit net'
three_unit.run()

print 'Creating classifiers'
three = Classifier(examples.attr, three_unit.hweights, three_unit.oweights)

print '\n\nClassifications for three hidden unit net on tennis training data'

print 'Inputs                       | Hidden         | Outputs | Expected | Actual'

correct = 0.0
for ex in examples.train:
    inputs, hidden, output, _ = three.classify(ex)

    inputs = ' '.join(inputs)
    hidden = ' '.join('%.2f' % h for h in hidden)
    output = ' '.join(str(o) for o in output)

    classification = 'No'
    if int(output) == 1:
        classification = 'Yes'

    prediction = 'No'
    if ex['PlayTennis'] == 1:
        prediction = 'Yes'

    if prediction == classification:
        correct += 1.0

    print '%s | %s | %s | %s | %s' % (inputs.ljust(28), hidden, output.center(7), prediction.center(8), classification.center(6))

print '\nAccuracy of %d%% on training data' % (100 * correct / len(examples.train))

print '\n\nClassifications for three hidden unit net on tennis test data'

correct = 0.0
for ex in examples.test:
    inputs, hidden, output, _ = three.classify(ex)

    inputs = ' '.join(inputs)
    hidden = ' '.join('%.2f' % h for h in hidden)
    output = ' '.join(str(o) for o in output)

    classification = 'No'
    if int(output) == 1:
        classification = 'Yes'

    prediction = 'No'
    if ex['PlayTennis'] == 1:
        prediction = 'Yes'

    if prediction == classification:
        correct += 1.0

    print '%s | %s | %s | %s | %s' % (inputs.ljust(28), hidden, output.center(7), prediction.center(8), classification.center(6))

print 'Accuracy of %d%% on test data' % (100 * correct / len(examples.test))


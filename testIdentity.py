from learner import Learner
from learner import Classifier
from utils import Examples
from utils import IDENTITY_TRAINING
from utils import IDENTITY_ATTR


LEARNING_RATE = 0.1
MOMENTUM = 0.02
TRIALS = 5000

print 'Training neural net with identity examples, a learning rate of %.3f, a momentum of %.3f, and %d trials' % (LEARNING_RATE, MOMENTUM, TRIALS)

examples = Examples(IDENTITY_ATTR, IDENTITY_TRAINING, IDENTITY_TRAINING)
three_unit = Learner(examples, 3, LEARNING_RATE, MOMENTUM, TRIALS)
four_unit = Learner(examples, 4, LEARNING_RATE, MOMENTUM, TRIALS)

# Training nets
print 'Running SGD for three hidden unit net'
three_unit.run()

print 'Running SGD for four hidden unit net'
four_unit.run()

print 'Creating classifiers'
three = Classifier(examples.attr, three_unit.hweights, three_unit.oweights)
four = Classifier(examples.attr, four_unit.hweights, four_unit.oweights)

print '\n\nClassifications for three hidden unit net'

print 'Inputs          | Hidden         | Outputs         | Classification'
for ex in examples.test:
    inputs, hidden, outputs, classification = three.classify(ex)

    inputs = ' '.join(inputs)
    hidden = ' '.join('%.2f' % h for h in hidden)
    outputs = ' '.join(str(o) for o in outputs)
    print '%s | %s | %s | %s' % (inputs, hidden, outputs, classification)

print '\n\nClassifications for four hidden unit net'

print 'Inputs          | Hidden             | Outputs         | Classification'
for ex in examples.test:
    inputs, hidden, outputs, classification = four.classify(ex)

    inputs = ' '.join(inputs)
    hidden = ' '.join('%.2f' % h for h in hidden)
    outputs = ' '.join(str(o) for o in outputs)
    print '%s | %s | %s | %s' % (inputs, hidden, outputs, classification)

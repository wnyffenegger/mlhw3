import re

CONTINUOUS = "continuous"
INPUT = "in"
OUTPUT = "out"

TENNIS_ATTR = 'data/tennis-attr.txt'
IRIS_ATTR = 'data/iris-attr.txt'
IDENTITY_ATTR = 'data/identity-attr.txt'

TENNIS_TEST = 'data/tennis-test.txt'
IRIS_TEST = 'data/iris-test.txt'

TENNIS_TRAINING = 'data/tennis-train.txt'
IRIS_TRAINING = 'data/iris-train.txt'
IDENTITY_TRAINING = 'data/identity-train.txt'


class Attributes:
    """
    Listing of attributes with possible values mapping to discrete values when necessary
    """
    def __init__(self, filename):
        """
        Load attributes from file, remembering order attributes appear in, then
        convert discrete attributes into integer outputs with declared mapping
        :param filename: name of file to load attributes from
        :type filename: str
        """

        self.attributes = list()    # list of attributes in order they appear in file
        self.continuous = dict()    # is attribute continuous or discrete
        self.inputs = list()        # list of input attributes
        self.attr_map = dict()      # mapping of attributes to other binary attributes (composite attributes)
        self.outputs = list()       # list of output attributes

        # Map attribute values to numbers in list
        self.forward_mapping = dict()

        # Map number representation of attributes back to values
        self.back_mapping = dict()

        with open(filename, 'r') as attr_file:

            lines = attr_file.readlines()
            total = len(lines)
            for line_number in range(0, total):
                line = lines[line_number]
                line = line.strip()

                # skip commented lines or empty lines
                if len(line) == 0 or line[0] == '#':
                    continue

                # otherwise tokenize line
                tokens = re.split(':*\\s+', line)

                name = tokens[0]
                values = tokens[1:]

                self.attributes.append(name)
                self.attr_map[name] = dict()
                # If continuous leave alone and mark as continuous (not expected)
                if values[0] == CONTINUOUS:
                    self.continuous[name] = True
                # If discrete build integer mapping
                else:
                    self.continuous[name] = False

                    if OUTPUT in name or line_number == len(lines) - 1:

                        if len(values) > 2:
                            for value in values:
                                self.build_mapping(name + '_' + value, ['Not ' + value, value])
                                self.attr_map[name][value] = name + '_' + value
                        else:
                            self.build_mapping(name, values)

                    else:
                        self.build_mapping(name, values)

                # Input add to list of input nodes otherwise add to list of output nodes
                if OUTPUT in name or line_number == len(lines) - 1:

                    if len(values) > 2:
                        for value in values:
                            self.outputs.append(name + '_' + value)
                    else:
                        self.outputs.append(name)
                else:
                    self.inputs.append(name)

        self.num_inputs = len(self.inputs)
        self.num_outputs = len(self.outputs)

    def build_mapping(self, name, values):
        """
        Build a backward and forward mapping matching a string to an integer value used for the network
        and an integer back to that string
        :param name: name of the attribute to which the values apply
        :type name: str
        :param values: possible string values of attribute
        :type values: list(str)
        :return:
        """

        self.forward_mapping[name] = dict()
        self.back_mapping[name] = dict()
        for index, val in enumerate(values):
            self.forward_mapping[name][val] = index
            self.back_mapping[name][index] = val

    def binary_mapping(self, name, values):
        self.forward_mapping[name] = dict()
        self.back_mapping[name] = dict()

        self.forward_mapping[name][values[0]] = 0.1
        self.back_mapping[name][0.1] = values[0]

        self.forward_mapping[name][values[1]] = 0.9
        self.back_mapping[name][0.9] = values[1]

    def forward_hash(self, attr_name, val):
        """
        Given an attribute name and value find the mapping to a discrete integer representation
        :param attr_name: the name of an attribute
        :type attr_name: str
        :param val: the value of an attribute
        :type val: str
        :return: int representing string in neural net
        :rtype: int
        """
        return self.forward_mapping[attr_name][val]

    def backward_hash(self, attr_name, val):
        """
        Given an attribute name and value find mapping to the string represented by the integer value
        :param attr_name: the name of an attribute
        :type attr_name: str
        :param val: value of an attribute
        :type val: int
        :return: string corresponding to integer value provided
        :rtype: str
        """
        return self.back_mapping[attr_name][val]


class Examples:

    def __init__(self, attr_file, train_file, test_file, validation=False):
        self.attr = Attributes(attr_file)
        self.train = Examples.build(self.attr, train_file)

        if validation:
            keep = int(0.67 * len(self.train))
            self.validation = self.train[keep:]
            self.train = self.train[:keep]
        else:
            self.validation = None

        if test_file is not None:
            self.test = Examples.build(self.attr, test_file)
        else:
            self.test = Examples.build(self.attr, train_file)

    @staticmethod
    def build(attr, filename):
        """
        Read file for all example data
        :param attr: attributes to build examples with
        :type attr: Attributes
        :param filename: name of file to read
        :type filename: str
        :return: values of all attributes with conversions of discrete values to integers already conducted
        :rtype: dict()
        """

        examples = list()
        with open(filename, 'r') as data:

            for line in data:
                line = line.strip()

                # Ignore empty lines or comments
                if len(line) == 0 or line[0] == '#':
                    continue

                # split all values on whitespace
                tokens = line.split()

                attr_values = zip(attr.attributes, tokens)

                example = dict()

                for attr_name, val in attr_values:

                    if attr.continuous[attr_name]:
                        example[attr_name] = float(val)
                    elif len(attr.attr_map[attr_name].keys()):

                        for v, bin_name in attr.attr_map[attr_name].iteritems():
                            if v == val:
                                example[bin_name] = 1
                            else:
                                example[bin_name] = 0
                    else:
                        example[attr_name] = attr.forward_hash(attr_name, val)

                examples.append(example)
        return examples

    @staticmethod
    def get(example, attributes):
        """
        Given a list of attribute names, retrieve all values and return them in order as an array
        :param example: set of attribute values composing an example
        :type example: dict()
        :param attributes: list of attributes to retrieve values for
        :type attributes: list(str)
        :return: list of values for attributes
        rtype: list(float)
        """
        return [float(example[a]) for a in attributes]


def test_attr():

    ident = Attributes(IDENTITY_ATTR)

    if len(ident.continuous.keys()) != 16:
        print "attributes not read in correctly should be sixteen attributes"

    if len(ident.inputs) != 8:
        print "attributes not read in correctly should be eight inputs"

    if len(ident.outputs) != 8:
        print "attributes not read in correctly should be eight outputs"

    if ident.continuous[ident.inputs[0]]:
        print "none of the identity variables should be continuous"

    if ident.forward_hash("in1", "0") != 0:
        print "mapping of attribute values to number failed"

    if ident.backward_hash("in1", 0) != "0":
        print "mapping of attribute values to number failed"

    if ident.forward_hash("in1", "1") != 1:
        print "mapping of attribute values to number failed"

    if ident.backward_hash("in1", 1) != "1":
        print "mapping of attribute values to number failed"

    tennis = Attributes(TENNIS_ATTR)

    if len(tennis.inputs) != 4:
        print "attributes not read in correctly should be four inputs"

    if len(tennis.outputs) != 1:
        print "attributes not read in correctly should be one output"

    if tennis.continuous[tennis.inputs[0]]:
        print "none of the identity variables should be continuous"

    if tennis.forward_hash("Outlook", "Sunny") != 0:
        print "mapping of attribute values to number failed"

    if tennis.backward_hash("Outlook", 0) != "Sunny":
        print "mapping of attribute values to number failed"

    if tennis.forward_hash("PlayTennis", "Yes") != 0:
        print "mapping of attribute values to number failed"

    if tennis.backward_hash("PlayTennis", 1) != "No":
        print "mapping of attribute values to number failed"

    iris = Attributes(IRIS_ATTR)

    if len(iris.inputs) != 4:
        print "attributes not read in correctly should be four inputs"

    if len(iris.outputs) != 3:
        print "attributes not read in correctly should be one output"

    if not iris.continuous[iris.inputs[0]]:
        print "none of the identity variables should be continuous"

    if iris.forward_hash("Iris_Iris-setosa", 'Not Iris-setosa') != 0:
        print "mapping of attribute values to number failed"

    if iris.backward_hash("Iris_Iris-setosa", 0) != 'Not Iris-setosa':
        print "mapping of attribute values to number failed"


def test_examples():

    ident = Examples(IDENTITY_ATTR, IDENTITY_TRAINING, IDENTITY_TRAINING)

    if len(ident.train) != 8:
        print "failed to read in all examples"

    if len(ident.train[0].keys()) != 16:
        print "failed to read in all attributes for example"

    tennis = Examples(TENNIS_ATTR, TENNIS_TRAINING, TENNIS_TEST)

    if len(tennis.train) != 10:
        print "failed to read in all examples"

    if len(tennis.test) != 4:
        print "failed to read in all examples"

    if len(tennis.train[0].keys()) != 5:
        print "failed to read in all attributes for example"

    iris = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST)

    if len(iris.train) != 100:
        print "failed to read in all examples"

    if len(iris.test) != 50:
        print "failed to read in all examples"

    if len(iris.train[0].keys()) != 7:
        print "failed to read in all attributes for example"

from learner import Learner
from learner import Classifier
from utils import Examples
from utils import IRIS_ATTR
from utils import IRIS_TRAINING
from utils import IRIS_TEST

# Constants for script
LEARNING_RATE = 0.1     # How much of error to adjust for
MOMENTUM = 0.02         # Momentum constant, how much of previous weight change to add to current weight change
TRIALS = 250            # Maximum number of iterations through all examples
OVERFIT = 3             # Percentage overfit to accept
HIDDEN = 3              # Number of hidden units in hidden layer
VALIDATE = True         # Should a validation set be used to test for overfitting

print 'Training neural net with identity examples, %d hidden units, a learning rate of %.3f, a momentum of %.3f,' \
      ' %d trials, %s validation, %d%% overfit threshold' % (HIDDEN, LEARNING_RATE, MOMENTUM, TRIALS, VALIDATE, OVERFIT)

examples = Examples(IRIS_ATTR, IRIS_TRAINING, IRIS_TEST, VALIDATE)
learner = Learner(examples, HIDDEN, LEARNING_RATE, MOMENTUM, TRIALS)

# Training nets
print 'Running SGD for %d hidden unit net' % HIDDEN
learner.run(OVERFIT)

print 'Creating classifiers'
classifier = Classifier(examples.attr, learner.hweights, learner.oweights)

print '\n\nClassifications for three hidden unit net using Iris training data'

print 'Inputs              | Hidden         | Output| Classification | Prediction'
correct = 0.0
for ex in examples.train:
    inputs, hidden, outputs, _ = classifier.classify(ex)

    classification = None
    prediction = None

    for index, attr_name in enumerate(classifier.attr.outputs):
        if outputs[index] == 1:
            classification = attr_name.split('_')[1].center(15)
            break

    for attr_name in classifier.attr.outputs:
        if ex[attr_name] == 1:
            prediction = attr_name.split('_')[1].center(15)

    if classification == prediction:
        correct += 1.0

    inputs = ' '.join('%.2f' % i for i in inputs)
    hidden = ' '.join('%.2f' % h for h in hidden)
    outputs = ' '.join(str(o) for o in outputs)
    print '%s | %s | %s | %s | %s' % (inputs, hidden, outputs, classification, prediction)

print '\nAccuracy of %d%% on training data with length %d' % ((100 * correct / len(examples.train)), len(examples.train))

print '\n\nClassifications for three hidden unit net using Iris test data'

print 'Inputs              | Hidden         | Output| Classification | Prediction'
correct = 0.0
for ex in examples.test:
    inputs, hidden, outputs, _ = classifier.classify(ex)

    classification = None
    prediction = None

    for index, attr_name in enumerate(classifier.attr.outputs):
        if outputs[index] == 1:
            classification = attr_name.split('_')[1].center(15)
            break

    for attr_name in classifier.attr.outputs:
        if ex[attr_name] == 1:
            prediction = attr_name.split('_')[1].center(15)

    if classification == prediction:
        correct += 1.0

    inputs = ' '.join('%.2f' % i for i in inputs)
    hidden = ' '.join('%.2f' % h for h in hidden)
    outputs = ' '.join(str(o) for o in outputs)
    print '%s | %s | %s | %s | %s' % (inputs, hidden, outputs, classification, prediction)

print '\nAccuracy of %d%% on test data with length %d' % ((100 * correct / len(examples.test)), len(examples.test))

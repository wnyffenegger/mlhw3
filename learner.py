from utils import Attributes
from utils import Examples
from utils import IDENTITY_ATTR
from utils import IDENTITY_TRAINING
from math import exp
from copy import deepcopy
import random

OFFSET = 1          # Index offset to leave room for X0 weight in weights matrices
X0 = 1.0            # Constant value of first input to every node
THRESHOLD = 0.000001    # Threshold for floating point math equality


def gen_weight():
    """
    Generate small random weight in range
    :return: small random weight
    :rtype: float
    """
    return random.uniform(-0.05, 0.05)


def zeros(i, j=None):
    """
    Generate either a single list or matrix
    :param i: if j is None number of rows, otherwise number of columns
    :type i: int
    :param j: number of columns if not None
    :type j: int | None
    :return:
    :rtype: list(float) | list(list(float))
    """
    if j is None:
        return [0 for _ in range(0, i)]
    else:
        matrix = list()
        for _ in range(0, i):
            matrix.append([0 for _ in range(0, j)])
        return matrix


def weight_vector(size):
    """
    Generate set of weights used by neuron to calculate sum of inputs.

    An additional element is added at the beginning of the list to simulate x0
    :param size: number of upstream units contributing to neuron
    :type size: int
    :return:
    """
    return [gen_weight() for _ in range(0, size + 1)]


def net(values, weights):
    """
    Multiply each corresponding value with its corresponding weight
    :param values: list of values
    :type values: list(float)
    :param weights: list of weights for those values
    :type weights: list(float)
    :return:
    """

    s = 1 * weights[0]
    intercept_less = weights[1:]

    if len(intercept_less) != len(values):
        print 'net error: values and weights should have same length'

    for v, w in zip(values, intercept_less):
        s += v * w

    return s


def sigmoid(x):
    """
    Sigmoid function
    :param x: input to run through squashing function
    :type x: float
    :return: sigmoid value
    :rtype: float
    """
    return 1.0 / (1.0 + exp(-x))


def deltaK(target, output):
    """
    Given the expected value of the output and the actual value of the output for output unit k
    calculate the gradient of the error of the output.

    error = (target - output)^2, error' = (target - output)(output') essentially
    :param target: output expected for given example
    :type target: float
    :param output: output received from feed forward for given example
    :type output: float
    :return: error in output unit k
    :rtype: float
    """
    return output * (1.0 - output) * (target - output)


def deltaH(oweights, delta_k, h):
    """
    Calculate gradient of hidden unit by finding contribution of hidden unit to gradient in output units
    :param oweights: set of weights used by the output units
    :type oweights: list(list(float))
    :param delta_k: gradient in
    :type delta_k: list(float)
    :param h: the index of the hidden unit starting with 1 (the zero index belongs to y-intercept like value)
    :type h: int
    :return: gradient of the error of the hidden unit h
    :rtype: float
    """
    s = 0.0
    for k in range(0, len(oweights)):
        s += oweights[k][h] * delta_k[k]
    return s


def feed_forward(inputs, hweights, oweights):
    """
    Given a list of inputs calculate the value of the outputs and return them.

    As a convenience always make the first weight in the list of inputs 1 to simulate w0 or the 'y-intercept'
    :param inputs: input values of an example
    :type inputs: list()
    :param hweights: matrix of current hidden layer weights
    :type hweights: list(list(float))
    :param oweights: matrix of current output layer weights
    :return: list of outputs from the network for the back propagation
    :rtype: tuple(list(float))
    """

    num_hidden = len(hweights)
    num_outputs = len(oweights)

    # Calculate value of all hidden units
    hidden = zeros(num_hidden)      # set value of hidden units to zero

    # instead of using list comprehension and pre-pending one element just initialize
    # hidden vector first and then fill in with for loop
    for i in range(0, num_hidden):
        n = net(inputs, hweights[i])
        hidden[i] = sigmoid(n)

    # Calculate and return value of all output units
    # don't need y-intercept weight
    outputs = zeros(num_outputs)
    for i in range(0, num_outputs):
        n = net(hidden, oweights[i])
        outputs[i] = sigmoid(n)
    return hidden, outputs


class Learner:

    def __init__(self, examples, num_hidden, learning_rate, alpha, iterations, decay=None):
        """
        Create learner with list of examples and provided parameters
        :param examples: set of examples and attributes to train with
        :type examples: Examples
        :param num_hidden: number of hidden units to use
        :type num_hidden: int
        :param learning_rate: number less than 0.1 which indicates jump in direction of error
        :type learning_rate: float
        :param alpha: momentum constant
        :type alpha: float
        :param iterations: max number of iterations to optimize over
        :type iterations: int
        :param decay: decay rate for weights
        :type decay: float
        """

        self.examples = examples
        self.attr = examples.attr
        self.num_hidden = num_hidden
        self.learning_rate = learning_rate
        self.alpha = alpha
        self.iterations = iterations
        self.decay = decay

        # Initialize layers to randomly weighted vectors for each unit in the hidden and output layers
        # include a y-intercept weight
        self.hweights = [weight_vector(self.attr.num_inputs) for _ in range(0, num_hidden)]
        self.oweights = [weight_vector(num_hidden) for _ in range(0, self.attr.num_outputs)]

        self.hweight_change = zeros(len(self.hweights), len(self.hweights[0]))
        self.oweight_change = zeros(len(self.oweights), len(self.oweights[0]))

    def back_propagation(self, inputs, target, outputs, hidden):
        """
        Given a list of target output values and actual output values
        :param inputs: input values from example
        :type inputs: list(float)
        :param target: target values derived from neural network
        :type target: list(float)
        :param outputs: outputs derived from neural network
        :type outputs: list(float)
        :param hidden: values of hidden units
        :type hidden: list(float)
        :return:
        """

        if len(outputs) != len(target):
            print 'outputs and targets not same length in back propagation'

        # Calculate the error in each of the output units
        delta_k = zeros(len(outputs))
        for i in range(0, len(outputs)):
            delta_k[i] = deltaK(target[i], outputs[i])

        # Calculate the error in each of the hidden units
        delta_h = zeros(len(hidden))
        for i in range(0, len(hidden)):
            delta_h[i] = hidden[i] * (1 - hidden[i]) * deltaH(self.oweights, delta_k, i + 1)

        # Calculate the weight updates necessary for both the output layer weights and the hidden layer weights
        self.oweight_change = self.update(hidden, self.oweights, delta_k, self.oweight_change)
        self.hweight_change = self.update(inputs, self.hweights, delta_h, self.hweight_change)

        # Update output layer weights and hidden layer weights
        for i in range(0, len(self.oweight_change)):
            for j in range(0, len(self.oweight_change[0])):
                self.oweights[i][j] += self.oweight_change[i][j]

        for i in range(0, len(self.hweight_change)):
            for j in range(0, len(self.hweight_change[0])):
                self.hweights[i][j] += self.hweight_change[i][j]

    def weight_change(self, delta_j, value):
        """
        Given the learning rate, error in downstream node, and current weight, calculate weight change.

        Standard weight update without momentum
        :param delta_j: error between target value of downstream node and actual value
        :type delta_j: float
        :param value: current value of input from upstream node * weight
        :type value: float
        :return: required change based on contribution
        :rtype: float
        """
        return self.learning_rate * delta_j * value

    def momentum(self, past):
        """
        Given a past weight change find momentum applied by that weight
        :param past:
        :type past: float
        :return: momentum
        :rtype: float
        """
        return self.alpha * past

    def update(self, inputs, weights, delta, past_weight_changes):
        """
        Given a list of inputs, weights, gradients of the error the inputs and weights produce,
        and past weights, update all of the provided weights
        :param inputs: values of all input nodes
        :type inputs: list(float)
        :param weights: matrix of weights where each row corresponds
        :type weights: list(list(float))
        :param delta: the errors produced by each row of weights in the corresponding downstream node
        :type delta: list(float)
        :param past_weight_changes: weight changes made by update function in past iteration
        :type past_weight_changes: list(list(float))
        :return: all changes to weights completed during current iteration
        :rtype: list(list(float))
        """

        weight_changes = zeros(len(weights), len(weights[0]))

        for j in range(0, len(weights)):

            # for y intercept weight
            change = self.weight_change(delta[j], X0)
            momentum = self.momentum(past_weight_changes[j][0])
            weight_changes[j][0] = change + momentum

            # for each input unit calculate necessary standard update and momentum
            for i in range(0, len(inputs)):
                change = self.weight_change(delta[j], inputs[i])
                momentum = self.momentum(past_weight_changes[j][i + OFFSET])
                weight_changes[j][i + OFFSET] = change + momentum

        return weight_changes

    def run(self, overfit=3):
        """
        For desired number of iterations run feed forward and back propagation as stochastic gradient descent
        :param overfit: tolerance between best classifier and current classifier before stopping
        :type overfit: float
        """

        copy_iteration = 0
        copy_oweights = self.oweights
        copy_hweights = self.hweights

        for i in range(0, self.iterations):

            for ex in self.examples.train:
                inputs = Examples.get(ex, self.attr.inputs)
                targets = Examples.get(ex, self.attr.outputs)
                hidden, outputs = feed_forward(inputs, self.hweights, self.oweights)
                self.back_propagation(inputs, targets, outputs, hidden)

            if self.examples.validation is not None:
                classifier = Classifier(self.attr, self.hweights, self.oweights)
                copy_classifier = Classifier(self.attr, copy_hweights, copy_oweights)

                comparison = Classifier.compare(self.attr, self.examples.validation, copy_classifier, classifier)
                if comparison <= 0:
                    copy_oweights = deepcopy(self.oweights)
                    copy_hweights = deepcopy(self.hweights)
                    copy_iteration = i

                elif comparison > overfit:

                    self.hweights = copy_hweights
                    self.oweights = copy_oweights

                    print 'Learner overfit after %d iterations reverting to copy after iteration %d' % (i, copy_iteration)
                    break


class Classifier:

    def __init__(self, attr, hidden_weights, output_weights):
        """
        Classify instances based on provided weights and spit back classification
        based on value of outputs on demand
        :param attr: set of attributes for examples, spit out classification based on
        :type attr: Attributes
        :param hidden_weights: set of weights between input and hidden layer
        :type hidden_weights: list(list(float))
        :param output_weights: set of weights between hidden and output layer
        :type output_weights: list(list(float))
        """
        self.attr = attr
        self.hidden_weights = hidden_weights
        self.output_weights = output_weights

    def classify(self, example):
        inputs = Examples.get(example, self.attr.inputs)
        targets = Examples.get(example, self.attr.outputs)
        hidden, outputs = feed_forward(inputs, self.hidden_weights, self.output_weights)

        normalized = [int(round(o)) for o in outputs]

        classification = None
        for i in range(0, len(normalized)):
            if normalized[i] == 1:
                classification = self.attr.outputs[i]

        back_inputs = list()
        for index, val in enumerate(inputs):

            if not self.attr.continuous[self.attr.inputs[index]]:
                back_inputs.append(self.attr.backward_hash(self.attr.inputs[index], val))
            else:
                back_inputs.append(val)

        # print 'attempting to classify %s as %s' % (str(inputs), str(targets))
        # print 'classified as %s' % str(normalized)

        return back_inputs, hidden, normalized, classification

    @staticmethod
    def compare(attr, examples, class1, class2):
        """
        Given two classifiers compare the accuracy on the given examples
        :param attr:
        :type attr: Attributes
        :param examples:
        :type examples: list(dict())
        :param class1:
        :type class1: Classifier
        :param class2:
        :type class2: Classifier
        :return: net difference in classification accuracy for the list of given examples
        :rtype: float
        """

        class1_correct = 0.0
        class2_correct = 0.0

        for ex in examples:
            predicted = None
            for o in attr.outputs:
                if ex[o] == 1:
                    predicted = o
                    break

            _, _, _, ex1_class = class1.classify(ex)
            _, _, _, ex2_class = class2.classify(ex)

            if ex1_class == predicted:
                class1_correct += 1.0

            if ex2_class == predicted:
                class2_correct += 1.0

        class1_accuracy = 100 * class1_correct / len(examples)
        class2_accuracy = 100 * class2_correct / len(examples)

        return class1_accuracy - class2_accuracy

def test_forward():
    """
    Test sigmoid, net, and feed forward functions with basic input
    :return:
    """

    # Test weight vector

    wv = weight_vector(2)

    if len(wv) != 3 or abs(wv[0]) > 0.05:
        print 'weight vector generator not functioning'

    # Test sigmoid
    zero = sigmoid(-100)
    half = sigmoid(0)
    one = sigmoid(100)

    if abs(half - 0.5) > THRESHOLD:
        print 'sigmoid not being calculated correctly'

    if abs(zero) > THRESHOLD:
        print 'sigmoid is not being calculated correctly'

    if abs(one - 1.0) > THRESHOLD:
        print 'sigmoid is not being calculated correctly'

    # Test net

    sum_prod = net([1, 2], [0, 2, 3])

    if abs(sum_prod - 8.0) > THRESHOLD:
        print 'net not being calculated correctly'

    # Test feed forward on simple net

    # Small net with set weights, should output 0.875 if calculations are done right
    inputs = [1, 1, 1]  # First input is x0 which is always one, next two are real inputs
    hweights = [[1, 1, 1, 1]]
    oweights = [[1, 1]]
    hidden, outputs = feed_forward(inputs, hweights, oweights)
    print hidden
    print outputs


def test_backward():
    """
    Test deltaK, deltaH, and back propagation methods with basic input
    :return:
    """

    # Test deltaK
    if abs(deltaK(0.5, 1)) > THRESHOLD:
        print 'deltaK not being calculated correctly'

    if abs(deltaK(0.5, 0)) > THRESHOLD:
        print 'deltaK not being calculated correctly'

    if abs(deltaK(0.5, 0.5)) > THRESHOLD:
        print 'deltaK not being calculated correctly'

    # Test deltaH
    oweights = [
        [0, 1.0],
        [0, 1.0]
    ]

    dk = [1.0, 1.0]

    if abs(deltaH(oweights, dk, 1) - 2.0) > THRESHOLD:
        print 'deltaH not being calculated correctly'

    identity = Examples(IDENTITY_ATTR, IDENTITY_TRAINING, IDENTITY_TRAINING)

    learner = Learner(identity, 4, 0.1, 0.01, 100)

    if len(learner.hweights) != 4:
        print 'hweights should have one vector per hidden unit'

    if len(learner.hweights[0]) != 9:
        print 'should be one dimension per input plus the y intercept input'

    if len(learner.oweights) != 8:
        print 'should be one vector per output unit'

    if len(learner.oweights[0]) != 5:
        print 'should be one dimension per hidden unit and the y intercept unit'


# integration test only
def test_learner():

    identity = Examples(IDENTITY_ATTR, IDENTITY_TRAINING, IDENTITY_TRAINING)

    learner = Learner(identity, 3, 0.1, 0.05, 5000)

    learner.run()

    if abs(learner.hweights[0][0]) < THRESHOLD and abs(learner.hweights[0][0]) < THRESHOLD:
        print "neural net weights converging to zero"

    classifier = Classifier(learner.attr, learner.hweights, learner.oweights)

    for ex in identity.test:
        classifier.classify(ex)
